console.log("Hello World!");

/*
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
Name (String)
Age (Number)
Pokemon (Array)
Friends (Object with Array values for properties)



*/


let details = {
	Name: "Ash Ketchum",
	age : 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends:
		{
			Hoenn: ["May", "Max"],
			Kanto: ["Brock", "Misty"]
		},
	talk: function()
		{

			console.log(this.pokemon[0] + "I choose you!")
		}	
}

console.log("Result of Dot Notation: ");
console.log(details.Name);
console.log("Result of square bracket Notation: ");
console.log(details["pokemon"]);
console.log("Result of talk method: ");
details.talk();


/*. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a constructor for creating a pokemon with the following properties:
Name (Provided as an argument to the constructor)
Level (Provided as an argument to the constructor)
Health (Create an equation that uses the level property)
Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon objects from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.

*/


function Pokemon(name, level){
	// Propeties
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	// Methods			 //result is object data type
	this.tackle = function(result){
				  //pokemonObject		//targetPokemon
		console.log(this.name +" tackled "+ result.name);
		// pokemonHealth = pokemon.health - pokemonAttack
		// 16 - 16 = 0
		let pokemonhealth = result.health - this.attack;

		console.log(result.name+ "health is reduced to " + pokemonhealth);

		// call faint method if the target pokemon's health is less than or equal to zero
		if(pokemonhealth <= 0){

			this.faint(result);
		}

	}
	this.faint = function(result){
		console.log(result.name+ " fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
/*

	health = 32;
	attack = 16;

*/

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("MewTwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);
			
